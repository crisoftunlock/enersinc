import React, { Component } from 'react';
import firebaseConfig from '../Firebase';

class Auth extends Component{

    state = {
        email: '',
        password: ''
    }

    actualizarState = (e) =>{
        this.setState({[e.target.name] : e.target.value})
    }

    login = e =>{
        console.log(firebaseConfig)
        e.preventDefault();
        const { email, password} = this.state;
        firebaseConfig.auth().signInWithEmailAndPassword(email, password).then((resultado) => {
            //window.location.replace('/');
        }).catch(error => console.log(error))
    }

    signup = e =>{
        e.preventDefault();
        const { email, password} = this.state;
        firebaseConfig.auth().createUserWithEmailAndPassword(email, password).then((resultado)=>{
        }).then((resultado)=>{console.log(resultado)})
        .catch((error) => {
            console.log(error);
          })
      }

      validarForm = () =>{
        if(this.state.email === '' || this.state.password ===''){
            return true;
        }
      }

    render(){
        return(
            <div className="row mt-5">
                <div className="col-12">
                    <form>
                        <div className="form-group">
                            <label>Correo electronico</label>
                            <input
                             onChange={this.actualizarState} 
                             type="text" name="email" className="form-control"/>
                        </div>
                        <div className="form-group">
                            <label>Contraseña</label>
                            <input
                             onChange={this.actualizarState} 
                             type="password" name="password" className="form-control"/>
                        </div>
                        <button disabled={this.validarForm()} type="submit" onClick={this.login} className="btn btn-primary float-right ml-5 btn-block">Entrar</button>
                        <button disabled={this.validarForm()} onClick={this.signup} className="btn btn-success float-right btn-block">Registrate</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Auth;