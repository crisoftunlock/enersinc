import React from 'react';
import firebaseConfig from '../Firebase';


const Navbar = ({usuario}) =>{
    
    const cerrarSesion = (e) =>{
        e.preventDefault();
        firebaseConfig.auth().signOut();
        localStorage.removeItem('usuario')
    }
    return( 
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-3 d-flex justify-content-center">
            <div id="navbarColor01">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    {(usuario) ? <span className="text-light">{usuario}<button onClick={cerrarSesion} className="btn btn-primary">Cerrar sesion</button></span> : ''}
                </li>
            </ul>
            </div>
        </nav>
    );
}

export default Navbar;