import React, { Component, Fragment } from 'react';
import firebaseConfig from '../Firebase';
import Loader from './Loader/Loader';

class Principal extends Component{
    state={
        nombre: '',
        apellido: '',
        correo: '',
        id:'',
        usuarios:[],
        cargando: true
    }

    componentDidMount(){
        firebaseConfig.firestore().collection('usuarios').onSnapshot(snapShot=>{
            this.setState({
                usuarios: snapShot.docs.map(doc =>{
                    return{id: doc.id, data: doc.data()}
                }),
                cargando: false
            })
        })
    }


    actualizarState = (e) =>{
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    validarForm = () =>{
        if(this.state.nombre === '' || this.state.apellido ==='' || this.state.correo === ''){
            return true;
        }
      }

    enviarFormulario = (e) =>{
        e.preventDefault();
        const { nombre, apellido, correo } = this.state;
        firebaseConfig.firestore().collection('usuarios').add({
            nombre,
            apellido,
            correo
        })
        .then(()=>{
            console.log('Agregado');
        })
        .catch((error)=>{
            console.log(error)
        })
        this.setState({
            nombre: '',
            apellido: '',
            correo: ''
        })
    }   

    eliminarUsuario = (id, usuario) =>{
        if(window.confirm(`Desea eliminar a ${usuario}`)){
            firebaseConfig.firestore().collection('usuarios').doc(id).delete()
        }
    }
    render(){
        const { usuarios, cargando } = this.state;
        if(cargando) return <Loader/>
        return(
            <Fragment>
                <div className="row">
                <div className="col-12 ">
                    <h2 className="text-center">Agregar usuario</h2>
                    <form onSubmit={this.enviarFormulario} className="form-inline d-flex justify-content-center">
                        <div className="form-group mr-3">
                            <input value={this.state.nombre} disabled={this.validarFormulario} name="nombre" placeholder="Nombre de usuario" className="form-control" onChange={this.actualizarState}/>
                        </div>
                        <div className="form-group mr-3">
                            <input value={this.state.apellido} name="apellido" placeholder="apellido" className="form-control" onChange={this.actualizarState}/>
                        </div>
                        <div className="form-group mr-3">
                            <input value={this.state.correo} name="correo" placeholder="correo electronico" className="form-control" onChange={this.actualizarState}/>
                        </div>
                        <button disabled={this.validarForm} type="submit" className="btn btn-primary">Agregar</button>
                    </form>
                </div>
                 </div>
                 <div className="row mt-5">
                    <div className="col-12">
                         <table class="table">
                             <thead>
                                 <tr>
                                     <th>Nombre</th>
                                     <th>apellido</th>
                                     <th>Email</th>
                                     <th>Eliminar</th>
                                 </tr>
                             </thead>
                             <tbody>
                               {usuarios.map((usuario, key)=>(
                                   <tr key={key}>
                                        <th>{usuario.data.nombre}</th>
                                        <th>{usuario.data.apellido}</th>
                                        <th>{usuario.data.correo}</th>
                                        <th><button onClick={()=>{this.eliminarUsuario(usuario.id, usuario.data.nombre)}} className="btn btn-danger">Eliminar</button></th>
                                   </tr>
                               ))}
                               </tbody>
                         </table>
                    </div>
                 </div>
            </Fragment>
        );
    }
}

export default Principal;