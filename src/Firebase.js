import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBharFgsNYdT9woyLvYXZg51fIwTMfian4",
    authDomain: "crud-enersinc.firebaseapp.com",
    databaseURL: "https://crud-enersinc.firebaseio.com",
    projectId: "crud-enersinc",
    storageBucket: "crud-enersinc.appspot.com",
    messagingSenderId: "134021021680",
    appId: "1:134021021680:web:f743a11d96406580c86735"
};

const firebaseConfig = firebase.initializeApp(config)
export default firebaseConfig;