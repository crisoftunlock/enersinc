import React, { Component, Fragment } from 'react';
import './bootstrap.min.css'
import firebaseConfig from './Firebase';
import Navbar from './Componentes/Navbar';
import Principal from './Componentes/Principal';
import Auth from './Componentes/Auth';

class App extends Component{
  state = {
    usuario: null,
    cargando: true
  }

  componentDidMount(){
    this.loginHandler();
  }

     loginHandler = () =>{
     firebaseConfig.auth().onAuthStateChanged(async usuario =>{

      if(usuario){
        this.setState({
          usuario : usuario
        })
        await localStorage.setItem('usuario', usuario.email)
      }else{
        this.setState({
          usuario: null
        });
        localStorage.removeItem('usuario')
      }
    })
  }

  render(){
    
    return(
      <Fragment>
        <Navbar usuario={localStorage.getItem('usuario')}/>
        <div className="container">
        <div>{this.state.usuario ? ( <Principal  usuario={localStorage.getItem('usuario')}/>) : (<Auth/>)}</div>
        </div>
      </Fragment>
    );
  }
}

export default App;